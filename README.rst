========================
django 1.8 CRUD
========================

A project CRud for Django 1.8 (based from http://www.pythondiary.com/tutorials/simple-crud-app-django.html).

To use this project follow these steps:

#. Create your working environment
#. clone repo
#. Install requirements.txt

*note: 


Working Environment
===================

You have several options in setting up your working environment.  We recommend
using virtualenv to separate the dependencies of your project from your system's
python environment.  If on Linux or Mac OS X, you can also use virtualenvwrapper to help manage multiple virtualenvs across different projects.

Virtualenv Only
---------------

First, make sure you are using virtualenv (http://www.virtualenv.org). Once
that's installed, create your virtualenv::

    $ virtualenv venv_crud

You will also need to ensure that the virtualenv has the project directory
added to the path. Adding the project directory will allow `django-admin.py` to
be able to change settings using the `--settings` flag.


Acknowledgements
================

- Many thanks to Kevin Veroneau.
- All of the contributors_ to this project.

.. _contributors: http://www.pythondiary.com/tutorials/simple-crud-app-django.html
