from django.contrib import admin

from people_app.models import Category, Person

admin.site.register(Category)
admin.site.register(Person)
