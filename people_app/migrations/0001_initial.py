# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(max_length=80)),
                ('slug', models.SlugField(blank=True)),
            ],
            options={
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('first_name', models.CharField(max_length=60, help_text='Their first name')),
                ('last_name', models.CharField(blank=True, max_length=80, help_text='Do you know it?')),
                ('slug', models.SlugField(blank=True)),
                ('proximity', models.PositiveSmallIntegerField(help_text='How much of a distance to keep?', choices=[(0, 'Not sure'), ('Friends', ((10, 'Best friends forever'), (20, 'Close friends'), (30, 'Distant friends'), (40, 'Enemies til the end'))), ('Family', ((50, 'Super close'), (60, 'Only on occasion'), (70, 'Only at funerals'), (80, 'Family grouch!')))])),
                ('home_phone', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128)),
                ('cell_phone', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128)),
                ('email', models.EmailField(blank=True, max_length=254, help_text='Do they have an Email address?')),
                ('added_on', models.DateField(auto_now_add=True)),
                ('category', models.ForeignKey(blank=True, help_text='File this person where?', to='people_app.Category', null=True)),
            ],
            options={
                'ordering': ['last_name'],
                'verbose_name_plural': 'people',
            },
        ),
    ]
